<?php

namespace Drupal\custom_action_links_csrf_tester;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\MetadataBag as CoreMetadataBag;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\State;

/**
 * Overrides the CSRF token seed to make testing a bit easier.
 */
class MetadataBag extends CoreMetadataBag {

  /**
   * The state key to store a token seed.
   */
  const STATE_KEY = 'custom_action_links_csrf_test.key';

  /**
   * @var \Drupal\Core\State\State
   */
  private $state;

  /**
   * MetadataBag constructor.
   *
   * @param \Drupal\Core\Site\Settings $settings
   * @param \Drupal\Core\State\State $state
   */
  public function __construct(Settings $settings, State $state) {
    parent::__construct($settings);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function setCsrfTokenSeed($csrf_token_seed) {
    // Use the static seed if available.
    $csrf_token_seed = $this->state->get(static::STATE_KEY) ?: $csrf_token_seed;
    $this->meta[static::CSRF_TOKEN_SEED] = $csrf_token_seed;
  }

  /**
   * {@inheritdoc}
   */
  public function getCsrfTokenSeed(): string {
    return $this->state->get(static::STATE_KEY) ?: parent::getCsrfTokenSeed();
  }

}
