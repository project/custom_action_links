<?php

namespace Drupal\Tests\custom_action_links\Functional;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\custom_action_links_csrf_tester\MetadataBag;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Custom Action Links functionality.
 *
 * @group custom_action_links
 */
class CustomActionLinksTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['custom_action_links', 'views_ui', 'block', 'node', 'custom_action_links_csrf_tester'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->createContentType(['type' => 'article']);
    $this->createContentType(['type' => 'page']);
  }

  /**
   * Tests the custom action links block.
   */
  public function testBlock() {
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    $block_admin_user = $this->createUser([], 'test-admin', TRUE);
    $this->drupalLogin($block_admin_user);
    $this->drupalget('admin/structure/block');
    $assert->elementExists('css', 'tr.region-title-content')->clickLink('Place block');
    $tr = $assert->elementExists('xpath', "//td[text()='Custom action links']")->getParent();
    $tr->clickLink('Place block');
    $assert->pageTextContains('Custom action link 1 details');
    $page->fillField('settings[label]', 'This is a block title');
    $page->fillField('settings[custom_action_links][0][route_details][route_name]', 'node.add');
    $page->fillField('settings[custom_action_links][0][route_details][route_parameters]', 'node_type=article');
    $link_text = 'Add an article<strong>!!!</strong>';
    $page->fillField('settings[custom_action_links][0][route_details][link_title]', $link_text);
    $page->pressButton('Save block');
    $assert->pageTextContains('The block configuration has been saved.');

    // Add another action link.
    $assert->elementExists('css', 'tr[data-drupal-selector="edit-blocks-customactionlinks"]')->clickLink('Configure');
    $page->fillField('settings[custom_action_links][1][route_details][route_name]', 'node.add');
    $page->fillField('settings[custom_action_links][1][route_details][route_parameters]', 'node_type=page');
    $page->pressButton('Save block');
    $assert->pageTextContains('The text for action link is required.');
    $page_link_text = 'Add page';
    $page->fillField('settings[custom_action_links][1][route_details][link_title]', $page_link_text);
    $page->pressButton('Save block');
    $assert->pageTextContains('The block configuration has been saved.');

    $this->drupalGet('');
    $assert->pageTextContains('This is a block title');
    $assert->assertEscaped($link_text);
    $assert->linkExists($page_link_text);
    $page->clickLink($link_text);
    $assert->statusCodeEquals(200);
    $assert->addressEquals('node/add/article');

    // This user does not have the correct permissions.
    $this->drupalLogin($this->createUser([]));
    $this->drupalGet('');
    $assert->linkNotExists($link_text);
    $assert->linkNotExists($page_link_text);
    $assert->pageTextNotContains('This is a block title');

    // Login as a user who has the permissions to one of the action links.
    $this->drupalLogin($this->createUser(['create article content']));
    $assert->pageTextContains('This is a block title');
    $assert->linkNotExists($page_link_text);
    $assert->assertEscaped($link_text);
    $page->clickLink($link_text);
    $assert->statusCodeEquals(200);
    $assert->addressEquals('node/add/article');

    // Anonymous has no access.
    $this->drupalLogout();
    $assert->linkNotExists($link_text);
    $assert->linkNotExists($page_link_text);
    $assert->pageTextNotContains('This is a block title');

    $this->drupalLogin($block_admin_user);
    $this->drupalget('admin/structure/block');
    $assert->elementExists('css', 'tr[data-drupal-selector="edit-blocks-customactionlinks"]')->clickLink('Configure');
    $page->fillField('settings[custom_action_links][0][route_details][route_parameters]', 'node_type="><script>alert(\'alert\')</script><a href="');
    $page->fillField('settings[custom_action_links][0][route_details][link_title]', '');
    $page->pressButton('Save block');
    $assert->pageTextContains('The text for action link is required');
    $assert->pageTextContains('The route parameters are incorrect:');
  }

  /**
   * Tests the custom action links views area.
   */
  public function testViewsArea() {
    $page = $this->getSession()->getPage();
    $assert = $this->assertSession();

    $this->drupalLogin($this->createUser(['administer views']));
    $this->drupalGet('admin/structure/views/add');
    $page->fillField('label', 'test_view');
    $page->checkField('page[create]');
    $page->pressButton('Save and edit');
    $page->fillField('id', 'test_view');
    $page->fillField('page[title]', 'Test view');
    $page->fillField('page[path]', '/test-view');
    $page->pressButton('Save and edit');
    $page->clickLink('views-add-header');
    $page->checkField('name[custom_action_links.custom_action_links]');
    $page->pressButton('Add and configure header');
    $page->checkField('options[empty]');
    $page->fillField('options[custom_action_links][0][route_details][route_name]', 'node.add');
    $page->fillField('options[custom_action_links][0][route_details][route_parameters]', 'node_type=article');
    $link_text = 'Add a page<strong>!!!</strong>';
    $page->fillField('options[custom_action_links][0][route_details][link_title]', $link_text);
    $page->pressButton('Apply');
    $page->pressButton('Save');

    // This user does not have the correct permissions.
    $this->drupalGet('test-view');
    $assert->linkNotExists($link_text);

    // Login as a user who has the permissions.
    $this->drupalLogin($this->createUser(['create article content']));
    $this->drupalGet('test-view');
    $assert->assertEscaped($link_text);
    $page->clickLink($link_text);
    $assert->statusCodeEquals(200);
    $assert->addressEquals('node/add/article');

    // Anonymous has no access.
    $this->drupalLogout();
    $this->drupalGet('test-view');
    $assert->linkNotExists($link_text);
  }

  /**
   * Tests the security of the autocomplete route.
   */
  public function testAutocompleteSecurity() {
    $assert = $this->assertSession();
    // Use the ability of the test module to set a CSRF static seed.
    \Drupal::state()->set(MetadataBag::STATE_KEY, Crypt::randomBytesBase64());

    // Not logged in. Valid user token and CSRF token.
    $user_token = Crypt::hmacBase64('0', Settings::getHashSalt() . \Drupal::service('private_key')->get());
    $url = "custom-access-links/routes/$user_token";
    $this->drupalGet($url, ['query' => ['q' => 'node', 'token' => \Drupal::service('csrf_token')->get($url)]]);
    $assert->statusCodeEquals(403);

    $user = $this->createUser([]);
    $this->drupalLogin($user);

    // Invalid user token. Valid CSRF token.
    $user_token = Crypt::randomBytesBase64();
    $url = "custom-access-links/routes/$user_token";
    $this->drupalGet($url, ['query' => ['q' => 'node', 'token' => \Drupal::service('csrf_token')->get($url)]]);
    $assert->statusCodeEquals(403);

    // Valid user token. Invalid CSRF token.
    $user_token = Crypt::hmacBase64($user->id(), Settings::getHashSalt() . \Drupal::service('private_key')->get());
    $url = "custom-access-links/routes/$user_token";
    $this->drupalGet($url, ['query' => ['q' => 'node', 'token' => 'this-is-not-valid']]);
    $assert->statusCodeEquals(403);

    // Valid user token and CSRF token.
    $this->drupalGet($url, ['query' => ['q' => 'node', 'token' => \Drupal::service('csrf_token')->get($url)]]);
    $assert->statusCodeEquals(200);
    // Assert a couple of route names are present. A full test would require
    // a WebDriverTestBase test.
    $assert->responseContains('node.add_page');
    $assert->responseContains('node.configure_rebuild_confirm');
  }

}
