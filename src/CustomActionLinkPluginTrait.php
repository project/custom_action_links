<?php

namespace Drupal\custom_action_links;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Provides common functionality for plugins to display custom action links.
 */
trait CustomActionLinkPluginTrait {
  use StringTranslationTrait;

  /**
   * Adds the route configure form to a form.
   *
   * @param array $form
   *   The form to add the route configure form to.
   * @param array $config
   *   Array of route configuration. Has the keys:
   *   - 'route_name'
   *   - 'route_parameters'
   *   - 'link_title'.
   *
   * @return array
   *   The form.
   */
  protected function buildRouteConfigureForm(array $form, array $config): array {
    // Create the form.
    $form['custom_action_links'] = [
      '#type' => 'container',
    ];
    $action_links = $config['custom_action_links'] ?? [];
    foreach ($action_links ?? [] as $key => $action_link) {
      $form['custom_action_links'][$key]['route_details'] = [
        '#type' => 'custom_action_link',
        '#title' => $this->t('Custom action link @number details', ['@number' => $key + 1]),
        '#open' => $key === 0 || !empty($action_link['route_name']),
        '#action_link' => $action_link,
        '#route_access_check' => FALSE,
      ];
    }

    // Add a empty action link to ensure we can add additional links.
    $key = count($action_links);
    $form['custom_action_links'][$key]['route_details'] = [
      '#type' => 'custom_action_link',
      '#title' => $this->t('Custom action link @number details', ['@number' => $key + 1]),
      '#open' => $key === 0 || !empty($values['route_name']),
      '#route_access_check' => FALSE,
    ];

    return $form;
  }

  /**
   * Converts configuration into a render array of action links.
   *
   * @param array $config
   *   The configuration.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The route access manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return array
   *   The render array.
   */
  private function toRenderArray(array $config, AccessManagerInterface $access_manager, AccountInterface $account): array {
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheContexts(['route']);
    foreach ($config['custom_action_links'] as $key => $values) {
      $route_name = $values['route_name'];
      $route_parameters = $values['route_parameters'];
      $access = $access_manager->checkNamedRoute($route_name, $route_parameters, $account, TRUE);
      $links[] = [
        '#theme' => 'menu_local_action',
        '#link' => [
          'title' => $values['link_title'],
          'url' => Url::fromRoute($route_name, $route_parameters),
          // @todo destination???
          // 'localized_options' => $plugin->getOptions($this->routeMatch),
        ],
        '#access' => $access,
      ];
      $cacheability->addCacheableDependency($access);
    }
    $cacheability->applyTo($links);

    return $links;
  }

  /**
   * Massages a form value into the expected value.
   *
   * @param array $custom_action_links
   *   An array of action links as produced by the render element.
   *
   * @return array
   *   An array of action links as expected by the configuration.
   *
   * @todo Refactor to be part of a render element.
   */
  private function massageCustomActionLinksValue(array $custom_action_links): array {
    foreach ($custom_action_links as &$action_link) {
      $action_link = $action_link['route_details'];
    }
    // Remove links with no route name.
    return array_filter($custom_action_links, function ($action_link) { return !empty($action_link['route_name']);});
  }
}
