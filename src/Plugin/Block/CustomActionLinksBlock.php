<?php

namespace Drupal\custom_action_links\Plugin\Block;

use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\custom_action_links\CustomActionLinkPluginTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display the custom action links.
 *
 * @Block(
 *   id = "custom_action_links",
 *   admin_label = @Translation("Custom action links")
 * )
 */
class CustomActionLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {
  use CustomActionLinkPluginTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The access manager.
   *
   * Classes that use this trait need to set this value in their constructor.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * CustomActionLinksBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The access manager.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, AccessManagerInterface $access_manager, AccountInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->accessManager = $access_manager;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('access_manager'),
      $container->get('current_user')
    );
  }

  /**
   * @inheritDoc
   */
  public function build(): array {
    return $this->toRenderArray($this->configuration, $this->accessManager, $this->account);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    return $this->buildRouteConfigureForm($form, $this->configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['custom_action_links'] = $this->massageCustomActionLinksValue($form_state->getValue('custom_action_links'));
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $access = parent::access($account, TRUE);
    // Display as long as one link is allowed. Links will be further access
    // checked in
    // \Drupal\custom_action_links\CustomActionLinkPluginTrait::toRenderArray().
    $final_route_access = AccessResult::forbidden('There are no accessible action links configured.');
    foreach ( $this->configuration['custom_action_links'] as $action_link) {
      $route_access = $this->accessManager->checkNamedRoute($action_link['route_name'], $action_link['route_parameters'], $this->account, TRUE);
      $access->addCacheableDependency($route_access);
      if ($route_access->isAllowed()) {
        $final_route_access = AccessResult::allowed();
      }
    }
    $access = $access->andIf($final_route_access);
    return $return_as_object ? $access : $access->isAllowed();
  }

}

